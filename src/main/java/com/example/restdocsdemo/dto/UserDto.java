package com.example.restdocsdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * gordeevnm@gmail.com
 * 10/7/19
 */
@Data
@AllArgsConstructor
public class UserDto {
    private String username;
    private String name;
}
