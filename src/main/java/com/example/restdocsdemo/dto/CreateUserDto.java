package com.example.restdocsdemo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * gordeevnm@gmail.com
 * 10/7/19
 */
@Data
@NoArgsConstructor
public class CreateUserDto {
    private String username;
    private String password;
}
