package com.example.restdocsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestdocsdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestdocsdemoApplication.class, args);
    }

}
